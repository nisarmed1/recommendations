<?php
require __DIR__ . '/../vendor/autoload.php';

use App\ConsoleApplication;

$argv[0] = dirname(__DIR__);
$app = new ConsoleApplication($argv);
$app->bootstrap();
$app->run();
