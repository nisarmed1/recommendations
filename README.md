# Movie Recommendations
This application gives movie recommendations for a given Genre and Showtime
## Setup
In order to run this application checkout this repository using git. Run following command once checkout is complete to install dependencies

	composer install

## Unit Tests
To run the Unit tests run following command

	./vendor/bin/phpunit tests

## Execution
This application can be executed in console and browser mode

 
### Console

In order to run this application in console, run the following command inside its root folder

	php console/console.php [genre] [time]

for example

	php console/console.php animation 12:00
	
### Browser

In order to run this application in browser, start the PHP standalone server using following command on its root folder
 
	php -S localhost:8000 -t public/
  
Open browser and type following in address bar  

http://localhost:8000/recommendations/animation/1200