<?php namespace App\Service;

use App\Repository\MovieRepositoryInterface;
use App\Model\Recommendation;

class MovieService
{
    private $repository;

    public function __construct(MovieRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Returns array of Recommendation object by filtering and converting
     * Movies returned by the repository
     *
     * @param mixed $genre
     * @param mixed $time
     *
     * Throws Exception if inputs are not valid
     *
     * @return array
     */
    public function getRecommendations($genre, $time)
    {
        if (preg_match('/^\w*$/', $genre) == false) {
            throw new \Exception('Invalid genre');
        }
        $inputTime = $time?new \DateTime($time):$time;
        if ($inputTime === false) {
            throw new \Exception('Invalid input time');
        }
        $movies = $this->repository->getAllMovies();
        $recommendations = [];
        foreach ($movies as $movie) {
            // filter movie showtimes which are 30 min ahead of input time
            $showings = array_filter($movie->getShowings(), function ($showing) use ($time) {
                // if time is empty return all showtimes
                if (!$time) {
                    return true;
                }
                $showingTime = new \DateTime($showing);
                $inputTime = new \DateTime($time);
                $offset = $showingTime->getOffset();
                // add timezone offset of showtime to input time as it would be in UTC
                return ($showingTime->format('U') - $inputTime->format('U') + $offset)/60 >= 30;
            });
            // get all genre if none specified in input
            $genre = $genre?:'.*';
            if (preg_grep("/^$genre$/i", $movie->getGenres()) && count($showings) > 0) {
                foreach ($showings as $showing) {
                    // create Recommendation object from Movie
                    $recommendations[] = new Recommendation($movie->getName(), $movie->getRating(), $showing);
                }
            }
        }
        // sort by ratings
        usort($recommendations, function ($a, $b) {
            return $b->getRating() - $a->getRating();
        });
        return $recommendations;
    }
}
