<?php namespace App;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class Application
{
    protected $container;
    protected $basePath;

    public function __construct($params)
    {
        $this->basePath = $params[0];
    }
    /**
     * runs the application. Fetch route based on the resource requested
     * and execute the controller
     *
     * @return void
     */
    public function run()
    {
        try {
            $context = new RequestContext('/');
            $matcher = new UrlMatcher($this->routes, $context);
            $route = $matcher->match($_SERVER['REQUEST_URI']);
            $controller = $this->container->get($route["_controller"]);
            switch ($route['_route']) {
                case 'recommendations':
                    $controller->index($route['genre'], $route['time']);
                    break;
            }
        } catch (\Exception $ex) {
            header('Content-Type: text/plain');
            print($ex->getMessage());
        }
    }
    /**
     * register DI Container and Routes
     *
     * @return void
     */
    public function bootstrap()
    {
        $this->container = $this->buildContainer();
        $this->routes = $this->buildRoutes();
    }
    /**
     * builds container based on the configuration file
     *
     * @return ContainerBuilder
     */
    public function buildContainer()
    {
        $container = new ContainerBuilder();
        $loader = new YamlFileLoader(
            $container,
            new FileLocator($this->basePath . '/config')
        );
        $loader->load('services.yaml');
        $container->compile();
        return $container;
    }
    /**
     * build routes
     *
     * @return RouteCollection
     */
    public function buildRoutes()
    {
        $route = new Route('/recommendations/{genre}/{time}', array('_controller' => 'App\Controller\MovieController'));
        $routes = new RouteCollection();
        $routes->add('recommendations', $route);
        return $routes;
    }
}
