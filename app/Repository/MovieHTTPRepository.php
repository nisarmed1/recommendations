<?php namespace App\Repository;

use GuzzleHttp\ClientInterface;
use Symfony\Component\Serializer\SerializerInterface;
use App\Model\Movie;

class MovieHTTPRepository implements MovieRepositoryInterface
{
    private $client;
    private $serializer;

    public function __construct(ClientInterface $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }
    /**
     * Returns array of Movie objects from an HTTP endpoint
     *
     * @return array
     */
    public function getAllMovies()
    {
        $response = $this->client->request('GET', 'raw/cVyp3McN');
        $data = $response->getBody()->getContents();
        /*
         * Use a serializer to deserialze JSON data into a list Movie objects
         */
        $movies = $this->serializer->deserialize($data, 'App\Model\Movie[]', 'json');
        return $movies;
    }
}
