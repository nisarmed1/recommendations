<?php namespace App\Model;

class Recommendation
{
    private $name;
    private $showing;
    private $rating;

    public function __construct($name, $rating, $showing)
    {
        $this->name = $name;
        $this->showing = $showing;
        $this->rating = $rating;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getRating()
    {
        return $this->rating;
    }
    public function getShowing()
    {
        return $this->showing;
    }
    public function toString()
    {
        $date = new \DateTime($this->showing);
        return "{$this->name}, showing at {$date->format('ga')}";
    }
}
