<?php namespace App\Model;

class Movie
{
    private $name;
    private $rating;
    private $genres;
    private $showings;
    public function __construct($name, $rating, $genres, $showings)
    {
        $this->name = $name;
        $this->rating = $rating;
        $this->genres = $genres;
        $this->showings = $showings;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
    public function getRating()
    {
        return $this->rating;
    }
    public function setGenres($genres)
    {
        $this->genres = $genres;
    }
    public function getGenres()
    {
        return $this->genres;
    }
    public function setShowings($showings)
    {
        $this->showings = $showings;
    }
    public function getShowings()
    {
        return $this->showings;
    }
}
