<?php namespace App\Controller;

use App\Service\MovieService;

class MovieController
{
    private $service;

    public function __construct(MovieService $service)
    {
        $this->service = $service;
    }
    /**
     * Controller's index function to render the output from the service
     *
     * @param string $genre
     * @param string $time
     * @return void
     */
    public function index(string $genre, string $time)
    {
        $recommendations = $this->service->getRecommendations($genre, $time);
        header('Content-Type: text/plain');
        if (count($recommendations) === 0) {
            print("No movie recommendations");
        }
        print(implode(array_map(function ($recommendation) {
            return $recommendation->toString();
        }, $recommendations), "\n"));
        print("\n");
    }
}
