<?php namespace App;

class ConsoleApplication extends Application
{
    protected $genre;
    protected $time;

    public function __construct($params)
    {
        $this->basePath = array_shift($params);
        $this->genre = addslashes(array_shift($params));
        $this->time = addslashes(array_shift($params));
    }
    /**
     * runs the application by executing controller
     *
     * @return void
     */
    public function run()
    {
        try {
            $controller = $this->container->get("App\Controller\MovieController");
            $controller->index($this->genre, $this->time);
        } catch (Exception $ex) {
            print($ex->getMessage());
        }
    }
}
