<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Application;

$options[] = dirname(__DIR__);
$app = new Application($options);
$app->bootstrap();
$app->run();
