<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Repository\MovieHTTPRepository;
use App\Service\MovieService;
use App\Model\Movie;

final class MovieServiceTest extends TestCase
{
    public function testRecommendationHasZootopia()
    {
        $stub = $this->createMock(MovieHTTPRepository::class);
        $stub->method("getAllMovies")
            ->willReturn([
                new Movie("Zootopia", 90, ["Animation", "Comedy", "Action & Adventure"], ["19:00+11:00"]),
                new Movie("Shaun The Sheep", 80, ["Animation", "Comedy"], ["19:00+11:00"])
                ]);
        
        $service = new MovieService($stub);
        $recommendations = $service->getRecommendations("animation", "12:00");
        $recommendations = array_filter($recommendations, function ($recommendation) {
            return $recommendation->getName() === "Zootopia";
        });
        $this->assertTrue(count($recommendations)>0);
    }
    public function testRecommendationDoesNotHaveZootopia()
    {
        $stub = $this->createMock(MovieHTTPRepository::class);
        $stub->method("getAllMovies")
            ->willReturn([
                new Movie("Zootopia", 90, ["Animation", "Comedy", "Action & Adventure"], ["19:00+11:00"]),
                new Movie("Shaun The Sheep", 80, ["Animation", "Comedy"], ["19:00+11:00"])
                ]);
        
        $service = new MovieService($stub);
        $recommendations = $service->getRecommendations("animation", "19:00");
        $recommendations = array_filter($recommendations, function ($recommendation) {
            return $recommendation->getName() === "Zootopia";
        });
        $this->assertTrue(count($recommendations)===0);
    }
    public function testAllRecommendationsReturned()
    {
        $stub = $this->createMock(MovieHTTPRepository::class);
        $stub->method("getAllMovies")
            ->willReturn([
                new Movie("Zootopia", 90, ["Animation", "Comedy", "Action & Adventure"], ["19:00+11:00"]),
                new Movie("Shaun The Sheep", 80, ["Animation", "Comedy"], ["19:00+11:00"])
                ]);
        
        $service = new MovieService($stub);
        $recommendations = $service->getRecommendations(null, null);
        $this->assertTrue(count($recommendations)===2);
    }
}
